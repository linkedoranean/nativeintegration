﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Byte[] UnityEngine.ImageConversion::EncodeToJPG(UnityEngine.Texture2D,System.Int32)
extern void ImageConversion_EncodeToJPG_mE289598FD45B6658A645B2CE359C4E6FC97BF875 (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	ImageConversion_EncodeToJPG_mE289598FD45B6658A645B2CE359C4E6FC97BF875,
};
static const int32_t s_InvokerIndices[1] = 
{
	152,
};
extern const Il2CppCodeGenModule g_UnityEngine_ImageConversionModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ImageConversionModuleCodeGenModule = 
{
	"UnityEngine.ImageConversionModule.dll",
	1,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
