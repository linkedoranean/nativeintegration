﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C (void);
// 0x0000000C System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75 (void);
// 0x0000000D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000010 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000011 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000012 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000013 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000014 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000015 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000018 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000019 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001A System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000001C System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x0000001D System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000024 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000027 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000002B System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000002C System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x0000002D System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000002E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000030 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000031 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000032 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000035 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000037 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003A System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000003B System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000003C System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x0000003D TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000003E System.Void System.Linq.Enumerable/<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228 (void);
// 0x0000003F System.Void System.Linq.Enumerable/<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032 (void);
// 0x00000040 System.Boolean System.Linq.Enumerable/<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10 (void);
// 0x00000041 System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D (void);
// 0x00000042 System.Void System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2 (void);
// 0x00000043 System.Object System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB (void);
// 0x00000044 System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4 (void);
// 0x00000045 System.Collections.IEnumerator System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880 (void);
// 0x00000046 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000047 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000048 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000004A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000004C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000004D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000004F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000050 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000051 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000052 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000053 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000057 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000058 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000059 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000005A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000005B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000005C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000005D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000005E System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000005F System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000060 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000061 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000062 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000063 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000064 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[100] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C,
	Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032,
	U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[100] = 
{
	0,
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	515,
	515,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	102,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[27] = 
{
	{ 0x02000004, { 42, 4 } },
	{ 0x02000005, { 46, 9 } },
	{ 0x02000006, { 57, 7 } },
	{ 0x02000007, { 66, 10 } },
	{ 0x02000008, { 78, 11 } },
	{ 0x02000009, { 92, 9 } },
	{ 0x0200000A, { 104, 12 } },
	{ 0x0200000B, { 119, 1 } },
	{ 0x0200000C, { 120, 2 } },
	{ 0x0200000E, { 122, 4 } },
	{ 0x0200000F, { 126, 21 } },
	{ 0x02000011, { 147, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 5 } },
	{ 0x06000007, { 25, 5 } },
	{ 0x06000008, { 30, 3 } },
	{ 0x06000009, { 33, 2 } },
	{ 0x0600000A, { 35, 3 } },
	{ 0x0600000D, { 38, 1 } },
	{ 0x0600000E, { 39, 3 } },
	{ 0x0600001E, { 55, 2 } },
	{ 0x06000023, { 64, 2 } },
	{ 0x06000028, { 76, 2 } },
	{ 0x0600002E, { 89, 3 } },
	{ 0x06000033, { 101, 3 } },
	{ 0x06000038, { 116, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[149] = 
{
	{ (Il2CppRGCTXDataType)2, 14118 },
	{ (Il2CppRGCTXDataType)3, 8416 },
	{ (Il2CppRGCTXDataType)2, 14119 },
	{ (Il2CppRGCTXDataType)2, 14120 },
	{ (Il2CppRGCTXDataType)3, 8417 },
	{ (Il2CppRGCTXDataType)2, 14121 },
	{ (Il2CppRGCTXDataType)2, 14122 },
	{ (Il2CppRGCTXDataType)3, 8418 },
	{ (Il2CppRGCTXDataType)2, 14123 },
	{ (Il2CppRGCTXDataType)3, 8419 },
	{ (Il2CppRGCTXDataType)2, 14124 },
	{ (Il2CppRGCTXDataType)3, 8420 },
	{ (Il2CppRGCTXDataType)2, 14125 },
	{ (Il2CppRGCTXDataType)2, 14126 },
	{ (Il2CppRGCTXDataType)3, 8421 },
	{ (Il2CppRGCTXDataType)2, 14127 },
	{ (Il2CppRGCTXDataType)2, 14128 },
	{ (Il2CppRGCTXDataType)3, 8422 },
	{ (Il2CppRGCTXDataType)2, 14129 },
	{ (Il2CppRGCTXDataType)3, 8423 },
	{ (Il2CppRGCTXDataType)2, 14130 },
	{ (Il2CppRGCTXDataType)3, 8424 },
	{ (Il2CppRGCTXDataType)3, 8425 },
	{ (Il2CppRGCTXDataType)2, 10271 },
	{ (Il2CppRGCTXDataType)3, 8426 },
	{ (Il2CppRGCTXDataType)2, 14131 },
	{ (Il2CppRGCTXDataType)3, 8427 },
	{ (Il2CppRGCTXDataType)3, 8428 },
	{ (Il2CppRGCTXDataType)2, 10278 },
	{ (Il2CppRGCTXDataType)3, 8429 },
	{ (Il2CppRGCTXDataType)2, 14132 },
	{ (Il2CppRGCTXDataType)3, 8430 },
	{ (Il2CppRGCTXDataType)3, 8431 },
	{ (Il2CppRGCTXDataType)2, 10284 },
	{ (Il2CppRGCTXDataType)3, 8432 },
	{ (Il2CppRGCTXDataType)2, 10285 },
	{ (Il2CppRGCTXDataType)2, 14133 },
	{ (Il2CppRGCTXDataType)3, 8433 },
	{ (Il2CppRGCTXDataType)2, 10288 },
	{ (Il2CppRGCTXDataType)2, 10290 },
	{ (Il2CppRGCTXDataType)2, 14134 },
	{ (Il2CppRGCTXDataType)3, 8434 },
	{ (Il2CppRGCTXDataType)3, 8435 },
	{ (Il2CppRGCTXDataType)3, 8436 },
	{ (Il2CppRGCTXDataType)2, 10295 },
	{ (Il2CppRGCTXDataType)3, 8437 },
	{ (Il2CppRGCTXDataType)3, 8438 },
	{ (Il2CppRGCTXDataType)2, 10307 },
	{ (Il2CppRGCTXDataType)2, 14135 },
	{ (Il2CppRGCTXDataType)3, 8439 },
	{ (Il2CppRGCTXDataType)3, 8440 },
	{ (Il2CppRGCTXDataType)2, 10309 },
	{ (Il2CppRGCTXDataType)2, 14042 },
	{ (Il2CppRGCTXDataType)3, 8441 },
	{ (Il2CppRGCTXDataType)3, 8442 },
	{ (Il2CppRGCTXDataType)2, 14136 },
	{ (Il2CppRGCTXDataType)3, 8443 },
	{ (Il2CppRGCTXDataType)3, 8444 },
	{ (Il2CppRGCTXDataType)2, 10319 },
	{ (Il2CppRGCTXDataType)2, 14137 },
	{ (Il2CppRGCTXDataType)3, 8445 },
	{ (Il2CppRGCTXDataType)3, 8446 },
	{ (Il2CppRGCTXDataType)3, 8074 },
	{ (Il2CppRGCTXDataType)3, 8447 },
	{ (Il2CppRGCTXDataType)2, 14138 },
	{ (Il2CppRGCTXDataType)3, 8448 },
	{ (Il2CppRGCTXDataType)3, 8449 },
	{ (Il2CppRGCTXDataType)2, 10331 },
	{ (Il2CppRGCTXDataType)2, 14139 },
	{ (Il2CppRGCTXDataType)3, 8450 },
	{ (Il2CppRGCTXDataType)3, 8451 },
	{ (Il2CppRGCTXDataType)3, 8452 },
	{ (Il2CppRGCTXDataType)3, 8453 },
	{ (Il2CppRGCTXDataType)3, 8454 },
	{ (Il2CppRGCTXDataType)3, 8080 },
	{ (Il2CppRGCTXDataType)3, 8455 },
	{ (Il2CppRGCTXDataType)2, 14140 },
	{ (Il2CppRGCTXDataType)3, 8456 },
	{ (Il2CppRGCTXDataType)3, 8457 },
	{ (Il2CppRGCTXDataType)2, 10344 },
	{ (Il2CppRGCTXDataType)2, 14141 },
	{ (Il2CppRGCTXDataType)3, 8458 },
	{ (Il2CppRGCTXDataType)3, 8459 },
	{ (Il2CppRGCTXDataType)2, 10346 },
	{ (Il2CppRGCTXDataType)2, 14142 },
	{ (Il2CppRGCTXDataType)3, 8460 },
	{ (Il2CppRGCTXDataType)3, 8461 },
	{ (Il2CppRGCTXDataType)2, 14143 },
	{ (Il2CppRGCTXDataType)3, 8462 },
	{ (Il2CppRGCTXDataType)3, 8463 },
	{ (Il2CppRGCTXDataType)2, 14144 },
	{ (Il2CppRGCTXDataType)3, 8464 },
	{ (Il2CppRGCTXDataType)3, 8465 },
	{ (Il2CppRGCTXDataType)2, 10361 },
	{ (Il2CppRGCTXDataType)2, 14145 },
	{ (Il2CppRGCTXDataType)3, 8466 },
	{ (Il2CppRGCTXDataType)3, 8467 },
	{ (Il2CppRGCTXDataType)3, 8468 },
	{ (Il2CppRGCTXDataType)3, 8091 },
	{ (Il2CppRGCTXDataType)2, 14146 },
	{ (Il2CppRGCTXDataType)3, 8469 },
	{ (Il2CppRGCTXDataType)3, 8470 },
	{ (Il2CppRGCTXDataType)2, 14147 },
	{ (Il2CppRGCTXDataType)3, 8471 },
	{ (Il2CppRGCTXDataType)3, 8472 },
	{ (Il2CppRGCTXDataType)2, 10377 },
	{ (Il2CppRGCTXDataType)2, 14148 },
	{ (Il2CppRGCTXDataType)3, 8473 },
	{ (Il2CppRGCTXDataType)3, 8474 },
	{ (Il2CppRGCTXDataType)3, 8475 },
	{ (Il2CppRGCTXDataType)3, 8476 },
	{ (Il2CppRGCTXDataType)3, 8477 },
	{ (Il2CppRGCTXDataType)3, 8478 },
	{ (Il2CppRGCTXDataType)3, 8097 },
	{ (Il2CppRGCTXDataType)2, 14149 },
	{ (Il2CppRGCTXDataType)3, 8479 },
	{ (Il2CppRGCTXDataType)3, 8480 },
	{ (Il2CppRGCTXDataType)2, 14150 },
	{ (Il2CppRGCTXDataType)3, 8481 },
	{ (Il2CppRGCTXDataType)3, 8482 },
	{ (Il2CppRGCTXDataType)3, 8483 },
	{ (Il2CppRGCTXDataType)3, 8484 },
	{ (Il2CppRGCTXDataType)2, 14151 },
	{ (Il2CppRGCTXDataType)2, 10410 },
	{ (Il2CppRGCTXDataType)2, 10408 },
	{ (Il2CppRGCTXDataType)2, 14152 },
	{ (Il2CppRGCTXDataType)3, 8485 },
	{ (Il2CppRGCTXDataType)2, 14153 },
	{ (Il2CppRGCTXDataType)3, 8486 },
	{ (Il2CppRGCTXDataType)3, 8487 },
	{ (Il2CppRGCTXDataType)3, 8488 },
	{ (Il2CppRGCTXDataType)2, 10414 },
	{ (Il2CppRGCTXDataType)3, 8489 },
	{ (Il2CppRGCTXDataType)3, 8490 },
	{ (Il2CppRGCTXDataType)2, 10417 },
	{ (Il2CppRGCTXDataType)3, 8491 },
	{ (Il2CppRGCTXDataType)1, 14154 },
	{ (Il2CppRGCTXDataType)2, 10416 },
	{ (Il2CppRGCTXDataType)3, 8492 },
	{ (Il2CppRGCTXDataType)1, 10416 },
	{ (Il2CppRGCTXDataType)1, 10414 },
	{ (Il2CppRGCTXDataType)2, 14155 },
	{ (Il2CppRGCTXDataType)2, 10416 },
	{ (Il2CppRGCTXDataType)3, 8493 },
	{ (Il2CppRGCTXDataType)3, 8494 },
	{ (Il2CppRGCTXDataType)3, 8495 },
	{ (Il2CppRGCTXDataType)2, 10415 },
	{ (Il2CppRGCTXDataType)3, 8496 },
	{ (Il2CppRGCTXDataType)2, 10428 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	100,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	27,
	s_rgctxIndices,
	149,
	s_rgctxValues,
	NULL,
};
